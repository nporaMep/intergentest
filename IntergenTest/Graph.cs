﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IntergenTest
{
	public class Graph<T> {
		#region Factory
		public Graph() {
			nodes = new List<GraphNode<T>> ();
		}
		#endregion
		#region Data
		readonly List<GraphNode<T>> nodes;
		#endregion
		#region Interface
		public void Add(GraphNode<T> node) {
			nodes.Add (node);
		}
		public GraphNode<T> Add(T value) {
			var res = new GraphNode<T> (value);
			Add (res);
			return res;
		}
		public void AddEdge(T from, T to, int cost) {
			var fromNode = GetOrCreateNode (from);
			var toNode = GetOrCreateNode (to);
			fromNode.Neighbors.Add (toNode);
			fromNode.Costs.Add (cost);
		}
		public GraphNode<T> GetNode(T value) {
			return nodes.FirstOrDefault (node => node.Value.Equals(value));
		}
		public GraphNode<T> GetOrCreateNode(T value) {
			var res = GetNode (value);
			if (res == null)
				res = Add (value);
			return res;
		}
		public int? GetCost(T from, T to) {
			var fromNode = GetNode (from);
			if (fromNode == null) return null;
			var toNode = GetNode (to);
			if (toNode == null) return null;
			return GetCost (fromNode, toNode);
		}
		public int? GetCost(GraphNode<T> from, GraphNode<T> to) {
			var index = from.Neighbors.FindIndex (val => val.Value.Equals(to.Value));
			if (index < 0) return null;
			return from.Costs [index];
		}
		public bool Contains(T value) {
			return nodes.Any(node => node.Value.Equals(value));
		}
		public bool Remove(T value) {
			var index = nodes.FindIndex (node => node.Value.Equals(value));
			if (index >= 0) {
				nodes.RemoveAt (index);
				return true;
			}
			return false;
		}
		public List<GraphNode<T>> Nodes { get { return nodes; } }
		#endregion
	}
}

