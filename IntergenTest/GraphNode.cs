﻿using System;
using System.Collections.Generic;

namespace IntergenTest
{
	public class GraphNode<T> {
		#region Factory
		public GraphNode(T value) {
			this.value = value;
		}
		#endregion
		#region Data
		readonly T value;
		List<GraphNode<T>> neighbors;
		List<int> costs;
		#endregion
		#region Interface
		public T Value { get { return value; } }
		public List<GraphNode<T>> Neighbors { 
			get { 
				if (neighbors == null)
					neighbors = new List<GraphNode<T>> (); 
				return neighbors;
			} }
		public List<int> Costs { 
			get { 
				if (costs == null)
					costs = new List<int> (); 
				return costs;
			} }
		public override string ToString () {
			return string.Format ("[GraphNode: Value={0}, Neighbors Count={1}]", Value, Neighbors.Count);
		}
		public override bool Equals (object obj) {
			if (obj is GraphNode<T>)
				return (obj as GraphNode<T>).value.Equals(value);
			return false;
		}
		public override int GetHashCode () {
			return value.GetHashCode ();
		}
		#endregion
	}
}

