﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IntergenTest {
	public class PathFinder<T> {
		#region Factory
		public PathFinder(bool enableLogging = false) {
			this.enableLogging = enableLogging;
		}
		#endregion
		#region Data
		bool enableLogging;
		PathFinderResult<T> result;
		#endregion
		#region Interface
		public PathFinderResult<T> FindNumberOfPaths(GraphNode<T> from, GraphNode<T> to, RouteSearchConstraints constraints) {
			if (!constraints.AreValid)
				throw new Exception ("Invalid constraints");

			result = new PathFinderResult<T> ();

			if (constraints.MaxDepth.HasValue)
				constraints.MaxDepth = constraints.MaxDepth - 1;
			FindNumberOfPathsRecursive (from, to, constraints, enableLogging ? new List<T>() : null);
			return result;
		}
		#endregion
		#region Methods
		void FindNumberOfPathsRecursive(GraphNode<T> fromNode, GraphNode<T> toNode, RouteSearchConstraints constraints, List<T> log) {
			if (constraints.MaxDepth.HasValue && constraints.MaxDepth.Value < 0) return;
			if (constraints.MaxDistance.HasValue && constraints.MaxDistance.Value < 0) return;

			if (enableLogging) {
				log = log.ToList ();
				log.Add (fromNode.Value);
			}

			for (int i = 0; i < fromNode.Neighbors.Count; i++) {
				var neighbor = fromNode.Neighbors [i];
				bool isPathFound = toNode.Equals (neighbor) && 
					(!constraints.MustBeExactDistance || (constraints.MustBeExactDistance && constraints.MaxDistance.HasValue && constraints.MaxDistance.Value == 0)) &&
					(!constraints.MustBeExactDepth || (constraints.MustBeExactDepth && constraints.MaxDepth.HasValue && constraints.MaxDepth.Value == 0));

				if (isPathFound)
					AddFoundPath (log, neighbor.Value);

				var newConstraints = constraints;
				if (newConstraints.MaxDepth.HasValue)
					newConstraints.MaxDepth = newConstraints.MaxDepth - 1;
				if (newConstraints.MaxDistance.HasValue)
					newConstraints.MaxDistance = newConstraints.MaxDistance - fromNode.Costs [i];
				FindNumberOfPathsRecursive (neighbor, toNode, newConstraints, log);
			}
		}
		void AddFoundPath(List<T> logBeforeLastStop, T lastStop) {
			if (enableLogging) {
				var successfulLog = logBeforeLastStop.ToList ();
				successfulLog.Add (lastStop);
				result.Paths.Add (successfulLog);
			}
			result.NumberOfPaths++;
		}
		#endregion
	}
}

