﻿using System.Collections.Generic;

namespace IntergenTest {
	public class PathFinderResult<T> {
		public PathFinderResult() {
			Paths = new List<List<T>> ();
		}
		public List<List<T>> Paths { get; set; }
		public int NumberOfPaths { get; set; }
	}
}
