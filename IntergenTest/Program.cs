﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IntergenTest {
	class MainClass {
		static RouteSystem routeSystem;
		public static void Main (string[] args) {
			while (true) {
				Console.Clear ();
				Console.WriteLine ("Press 1 for Test Task Input, 2 for Unit Tests");
				Console.WriteLine ("Press ESC to exit");
				var readkey = Console.ReadKey ();
				Console.WriteLine ();
				if (readkey.Key == ConsoleKey.Escape)
					break;
				if (readkey.Key == ConsoleKey.D1)
					TestTaskInput ();
				if (readkey.Key == ConsoleKey.D2)
					UnitTests ();

				Console.WriteLine ("Press any key to return to main menu...");
				Console.ReadKey ();
			}
		}
		static void TestTaskInput() {
			routeSystem = new RouteSystem ();
			routeSystem.AddRoutes ("AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7");

			LogRouteDistance ("ABC");
			LogRouteDistance ("AD");
			LogRouteDistance ("ADC");
			LogRouteDistance ("AEBCD");
			LogRouteDistance ("AED");
			Console.WriteLine ();

			LogFindNumberOfPaths ("C", "C", new RouteSearchConstraints () { MaxDepth = 3 });
			Console.WriteLine ();

			LogFindNumberOfPaths ("A", "C", new RouteSearchConstraints () { MaxDepth = 4, MustBeExactDepth = true });
			Console.WriteLine ();

			LogShortestRoute ("A", "C");
			LogShortestRoute ("B", "B");

			LogFindNumberOfPaths ("C", "C", new RouteSearchConstraints () { MaxDistance = 30 });
		}

		static void UnitTests() {
			Console.WriteLine ();
			UT1 ();
			Console.WriteLine ();
			UT2 ();
			Console.WriteLine ();
			UT3 ();
			Console.WriteLine ();
			UT4 ();
		}

		static void UT1() {
			Console.WriteLine ("Edge cases for route length");
			routeSystem = new RouteSystem ();
			LogRouteDistance ("AB");
			LogRouteDistance (null);
			LogRouteDistance (string.Empty);
			LogRouteDistance ("A");

			routeSystem = new RouteSystem();
			routeSystem.AddRoutes("AB2", "BC3", "CA7");
			LogRouteDistance ("XZ");
			LogRouteDistance ("CB");
			LogRouteDistance ("CAB");
		}
		static void UT2() {
			Console.WriteLine ("Edge cases for number of round trip routes with maximum stops");
			routeSystem = new RouteSystem ();
			LogFindNumberOfPaths ("C", "C", new RouteSearchConstraints () { MaxDepth = 0 });

			routeSystem = new RouteSystem ();
			routeSystem.AddRoutes ("CA5", "CB4", "AD6", "BD7", "DC5");
			LogFindNumberOfPaths ("C", "C", new RouteSearchConstraints () { MaxDepth = 0 });
			LogFindNumberOfPaths ("C", "C", new RouteSearchConstraints () { MaxDepth = -1 });

			LogFindNumberOfPaths ("AA", "AA", new RouteSearchConstraints () { MaxDepth = 50 });
		}
		static void UT3() {
			Console.WriteLine ("Edge cases for shortest route");
			routeSystem = new RouteSystem ();
			LogShortestRoute ("A", "C");
			LogShortestRoute ("A", "A");

			routeSystem = new RouteSystem ();
			routeSystem.AddRoutes ("CA5", "CB4", "AD6", "BD7", "DC5");
			LogShortestRoute ("A", "Z");
			LogShortestRoute ("Z", "A");

			LogShortestRoute ("AA", "BB");
		}
		static void UT4() {
			Console.WriteLine ("Edge cases for number of routes with exact stops");
			routeSystem = new RouteSystem ();
			LogFindNumberOfPaths ("C", "A", new RouteSearchConstraints () { MaxDepth = 0 });
			LogFindNumberOfPaths ("C", "A", new RouteSearchConstraints () { MaxDepth = 1 });
			LogFindNumberOfPaths ("C", "A", new RouteSearchConstraints () { MaxDepth = -5 });

			routeSystem = new RouteSystem ();
			routeSystem.AddRoutes ("CA5", "CB4", "AD6", "BD7", "DC5");
			LogFindNumberOfPaths ("C", "A", new RouteSearchConstraints () { MaxDepth = 0 });
			LogFindNumberOfPaths ("C", "A", new RouteSearchConstraints () { MaxDepth = 1 });
			LogFindNumberOfPaths ("C", "A", new RouteSearchConstraints () { MaxDepth = 2 });
			LogFindNumberOfPaths ("C", "A", new RouteSearchConstraints () { MaxDepth = 3 });
			LogFindNumberOfPaths ("C", "A", new RouteSearchConstraints () { MaxDepth = 4 });
			LogFindNumberOfPaths ("C", "A", new RouteSearchConstraints () { MaxDepth = -1 });

		}

		static void LogRouteDistance(string route) {
			var length = routeSystem.FindRouteDistance (route);
			Console.WriteLine ("route {0} - {1}", route ?? "null", length.HasValue ? length.Value.ToString () : "NO SUCH ROUTE");
		}
		static void LogShortestRoute(string from, string to) {
			var shortestPath = routeSystem.FindShortestRoute (from, to);
			Console.WriteLine ("from {0} to {1} shortest - {2}", from, to, shortestPath == null ? "PATH NOT FOUND" : shortestPath.PathLength.ToString());
		}
		static void LogFindNumberOfPaths(string from, string to, RouteSearchConstraints constraints) {
			var numberOfTrips = routeSystem.FindNumberOfRoutes (from, to, constraints);

			Console.WriteLine ();
			Console.WriteLine ("from {0} to {3} with max {1} distance AND with max {4} depth: {2}", from, constraints.MaxDistance, numberOfTrips.NumberOfPaths, to, constraints.MaxDepth);
			foreach (var route in numberOfTrips.Paths)
				Console.WriteLine (route.Aggregate (string.Empty, (res, routeStop) => res += routeStop));
		}
	}
}