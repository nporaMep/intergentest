﻿namespace IntergenTest {
	public struct RouteSearchConstraints {
		public int? MaxDepth;
		public bool MustBeExactDepth;
		public int? MaxDistance;
		public bool MustBeExactDistance;

		public bool AreValid { get { return MaxDepth.HasValue || MaxDistance.HasValue; } }
	}
}

