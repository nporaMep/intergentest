﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IntergenTest {
	public class RouteSystem {
		#region Factory
		#endregion
		#region Data
		Graph<string> _graph;
		#endregion
		#region Interface
		public void AddRoutes(params string[] routes) {
			foreach (var route in routes)
				AddRoute (route);
		}
		public void AddRoute(string route) {
			var parsedRoute = GetParsedRoute (route);
			if (parsedRoute == null) return;
			graph.AddEdge (parsedRoute.Item1, parsedRoute.Item2, parsedRoute.Item3);
		}
		public int? FindRouteDistance(string route) {
			if (string.IsNullOrEmpty (route)) return null;
			var splitRoute = SplitRoute (route);
			if (splitRoute == null) return null;
			var pathsEnumerator = splitRoute.GetEnumerator ();
			pathsEnumerator.MoveNext ();

			int? accumulatedPath = null;
			GraphNode<string> fromNode = graph.GetNode (pathsEnumerator.Current);
			GraphNode<string> toNode = null;
			while (pathsEnumerator.MoveNext()) {
				if (fromNode == null) return null;
				toNode = graph.GetNode (pathsEnumerator.Current);
				if (toNode == null) return null;

				var cost = graph.GetCost (fromNode, toNode);
				if (!cost.HasValue) return null;
				if (!accumulatedPath.HasValue)
					accumulatedPath = 0;
				accumulatedPath += cost.Value;

				fromNode = toNode;
			}
			return accumulatedPath;
		}
		public ShortestPathResult<string> FindShortestRoute(string from, string to) {
			var shortestPath = new ShortestPath <string>(graph);
			return shortestPath.FindShortestPath (from, to);
		}
		public PathFinderResult<string> FindNumberOfRoutes(string from, string to, RouteSearchConstraints constraints) {
			var pathFinder = new PathFinder<string> (true);
			var fromNode = graph.GetNode (from);
			if (fromNode == null) return new PathFinderResult<string> ();
			var toNode = graph.GetNode (to);
			if (toNode == null) return new PathFinderResult<string>();

			return pathFinder.FindNumberOfPaths (fromNode, toNode, constraints);
		}
		public IEnumerable<string> GetRoutes() {
			return graph.Nodes.SelectMany (node => node.Neighbors.Select ((n, i) => node.Value + n.Value + node.Costs [i].ToString ()));
		}
		#endregion
		#region Methods
		Graph<string> graph {
			get { 
				if (_graph == null) 
					_graph = new Graph<string>(); 
				return _graph;
			}
		}
		Tuple<string, string, int> GetParsedRoute(string route) {
			if (string.IsNullOrEmpty (route)) return null;
			if (route.Length < 2) return null;
			return new Tuple<string, string, int> (route.Substring (0, 1), route.Substring (1, 1), int.Parse (route.Substring (2)));
		}
		IEnumerable<string> SplitRoute(string route) {
			for (int i = 0; i < route.Length; i++) {
				yield return route.Substring (i, 1);
			}
		}
		#endregion
	}
}

