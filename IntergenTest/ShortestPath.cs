﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IntergenTest {
	public class ShortestPath<T> {
		#region Factory
		public ShortestPath (Graph<T> graph) {
			this.graph = graph;
			distanceMap = new Dictionary<GraphNode<T>, int> ();
			routeMap = new Dictionary<GraphNode<T>, GraphNode<T>> ();
		}
		#endregion
		#region Data
		Graph<T> graph;
		Dictionary<GraphNode<T>, int> distanceMap;
		Dictionary<GraphNode<T>, GraphNode<T>> routeMap;
		List<GraphNode<T>> queue;
		ShortestPathResult<T> result;
		#endregion
		#region Interface
		public ShortestPathResult<T> FindShortestPath(T from, T to) {
			var fromNode = graph.GetNode (from);
			if (fromNode == null) return null;
			var toNode = graph.GetNode (to);
			if (toNode == null) return null;

			result = new ShortestPathResult<T> ();
			if (from.Equals(to)) {
				FindShortestRoundTrip (fromNode);
			} else {
				FindShortestTrip (fromNode, toNode);
			}
			return result;
		}
		#endregion
		#region Methods
		void FindShortestTrip(GraphNode<T> from, GraphNode<T> to) {
			FindShortestPath (from, to);
		}
		void FindShortestRoundTrip(GraphNode<T> node) {
			int minLength = int.MaxValue;
			for (int i = 0; i < node.Neighbors.Count; i++) {
				var neighbor = node.Neighbors [i];
				FindShortestPath (neighbor, node);
				if (result.PathLength + node.Costs [i] < minLength) {
					minLength = result.PathLength + node.Costs [i];
				}
			}
			result.PathLength = minLength;
		}
		void FindShortestPath(GraphNode<T> from, GraphNode<T> to) {
			distanceMap = new Dictionary<GraphNode<T>, int> ();
			foreach (var node in graph.Nodes) {
				distanceMap [node] = node == from ? 0 : int.MaxValue;
				routeMap [node] = null;
			}

			queue = graph.Nodes.ToList ();
			while (queue.Count > 0) {
				int minDistance = int.MaxValue;
				GraphNode<T> minNode = null;
				foreach (var q in queue) {
					var qDistance = distanceMap [q];
					if (qDistance < minDistance) {
						minDistance = qDistance;
						minNode = q;
					}
				}
				if (minNode == null)
					break;
				queue.Remove (minNode);

				if (minNode.Neighbors != null) {
					for (int i = 0; i < minNode.Neighbors.Count; i++) {
						if ((minNode.Costs [i] + minDistance) < distanceMap [minNode.Neighbors [i]]) {
							distanceMap [minNode.Neighbors [i]] = minNode.Costs [i] + minDistance;
							routeMap [minNode.Neighbors [i]] = minNode;
						}
					}
				}
			}

			result.PathLength = distanceMap [to];
		}
		#endregion
	}
}

